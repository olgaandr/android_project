package com.example.lunarlandergame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Random;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Created by Olga on 26.3.14.
 */
public class LunarLanderView extends SurfaceView implements SurfaceHolder.Callback{
    Bitmap landerBitmap;
    float x;
    float y;
    float speed = 1;
    int displayWidth;
    int displayHeight;
    Paint paint = new Paint();
    SurfaceHolder surfaceHolder;
    int scaledBitmapWidth = 100;
    Thread thread;
    boolean slowDown = false;
    long time=0;

    public LunarLanderView(Context context, Bitmap bitmap) {
        super(context);
        //DisplayMetrics displayMetrics = new DisplayMetrics();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        displayWidth = displayMetrics.widthPixels;
        displayHeight = displayMetrics.heightPixels;
        //MainActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        Random r = new Random();
        //x = displayMetrics.widthPixels/2;
        x = (float) r.nextInt(displayWidth-scaledBitmapWidth);
        y = (float) r.nextInt(displayHeight/3);
        //Log.i("MyString",displayWidth+", x: "+x);

        //int bitmapHeightAndWidth = (int) getResources().getDimension(R.dimen.image_height);
        //landerBitmap = Bitmap.createScaledBitmap(bitmap, bitmapHeightAndWidth, bitmapHeightAndWidth, false);
        landerBitmap = Bitmap.createScaledBitmap(bitmap, scaledBitmapWidth, scaledBitmapWidth, false);
        paint.setAntiAlias(true);
        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);


    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        thread = new Thread(new Runnable() {
            public void run() {
                Canvas canvas = null;
                while (!Thread.currentThread().isInterrupted()) {
                    canvas = surfaceHolder.lockCanvas();
                    if (null != canvas) {
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {

                        }
                        //time++;
                        drawLander(canvas);
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        });
        thread.start();
    }

    public void slowDown(boolean b){
        slowDown = b;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        thread.interrupt();
    }

    public void drawLander(Canvas canvas){
        canvas.drawColor(Color.DKGRAY);
        canvas.drawBitmap(landerBitmap,x,y,paint);
        //if(time%10 == 0){
            if(!slowDown){
                speed++;
            }
            else{
                speed--;
            }
        //}

        y = y + speed;
    }
}
